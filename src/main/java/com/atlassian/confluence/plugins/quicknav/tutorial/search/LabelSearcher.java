package com.atlassian.confluence.plugins.quicknav.tutorial.search;

import com.atlassian.confluence.search.actions.json.ContentNameMatch;
import com.atlassian.confluence.search.contentnames.QueryToken;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.RelevanceSort;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

@Component
public class LabelSearcher {
    private final SearchManager searchManager;
    private final SearchResultTransformer transformer;

    @Autowired
    public LabelSearcher(@ComponentImport SearchManager searchManager,
                         SearchResultTransformer transformer) {
        this.searchManager = requireNonNull(searchManager);
        this.transformer = requireNonNull(transformer);
    }

    public List<ContentNameMatch> getMatches(List<QueryToken> tokens, int maxResults) {
        String query = tokens.stream().map(QueryToken::getText).collect(Collectors.joining(" "));
        SearchResults result;
        try {
            result = searchManager.search(new ContentSearch(
                    new LabelQuery(query),
                    new RelevanceSort(),
                    SiteSearchPermissionsSearchFilter.getInstance(),
                    0, maxResults));
        } catch (InvalidSearchException e) {
            throw new RuntimeException(e);
        }

        List<ContentNameMatch> matches = new ArrayList<>();
        result.forEach(x -> matches.add(transformer.transform(x)));

        return matches;
    }
}
