package com.atlassian.confluence.plugins.quicknav.tutorial;

import com.atlassian.confluence.plugins.quicknav.tutorial.search.LabelSearcher;
import com.atlassian.confluence.search.contentnames.ContentNameSearchContext;
import com.atlassian.confluence.search.contentnames.ContentNameSearchSection;
import com.atlassian.confluence.search.contentnames.ContentNameSearchSectionsProvider;
import com.atlassian.confluence.search.contentnames.QueryToken;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.requireNonNull;

public class LabelSearchSectionProvider implements ContentNameSearchSectionsProvider {
    private static final Integer WEIGHT = 6;
    private static final int MAX_RESULTS = 3;

    private final LabelSearcher labelSearcher;

    @Autowired
    public LabelSearchSectionProvider(LabelSearcher labelSearcher) {
        this.labelSearcher = requireNonNull(labelSearcher);
    }

    @Override
    public Collection<ContentNameSearchSection> getSections(List<QueryToken> tokens,
                                                            ContentNameSearchContext contentNameSearchContext) {
        return Collections.singletonList(
                new ContentNameSearchSection(WEIGHT, labelSearcher.getMatches(tokens, MAX_RESULTS)));
    }
}
